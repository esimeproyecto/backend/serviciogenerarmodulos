const fse = require('fs-extra');
let htmlSecondPart;
let firstPartSearch;
let thirdPartSearch;
let secondPartSearch;
let htmlPartSearch;

function initialize() {
    firstPartSearch =
        `import { Component, OnInit, ViewChild } from '@angular/core';
import { SpinnerService } from '../../../services/spinner-service.service';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import { DataTableService } from '../../../services/data-table.service';
import {MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router' ;\n
`;

    thirdPartSearch = ``;

    secondPartSearch = ``;

    htmlPartSearch =
        `<div class="text-center">
<mat-spinner [diameter]="50" *ngIf="spinnerService.visibility | async"></mat-spinner>
</div>
<div class="container-fluid" style="margin-top: 20px;">
<label>Filtro: </label>
<mat-form-field style="padding-left: 10px;">
    <mat-label>Escribe para filtrar en la tabla</mat-label>
    <input matInput (keyup)="applyFilter($event)">
</mat-form-field>

<div class="text-right">
    <button mat-fab color="primary" (click)="Agregar();" >
    <mat-icon>add</mat-icon>
    </button>
</div>

<table mat-table [dataSource]="dataSource" class="mat-elevation-z8" style="margin-top: 20px;"> \n`

    htmlSecondPart = ``;
}



function generateHTML(json) {
    htmlSecondPart = ``;
    for (let i = 0; i < json.columns.length; i++) {
        htmlSecondPart +=
            `<ng-container matColumnDef="${json.columns[i].Field || json.columns[i].field}">
            <th mat-header-cell *matHeaderCellDef> ${json.columns[i].Field || json.columns[i].field} </th>
            <td mat-cell *matCellDef="let element; let i = index">
                <a (click)="update(element)">{{element.${json.columns[i].Field || json.columns[i].field}}}</a>
            </td>
        </ng-container>\n`
    }

    htmlSecondPart +=
        `<ng-container matColumnDef="trash">
            <th mat-header-cell *matHeaderCellDef>Eliminar</th>
            <td mat-cell *matCellDef="let element; let i = index">
                <i  (click)="openSnackBarDEL(element)" class="far fa-trash-alt"></i>
            </td>
        </ng-container>\n
        
        <tr mat-header-row *matHeaderRowDef="displayedColumns"></tr>
        <tr mat-row *matRowDef="let row; columns: displayedColumns;"></tr>

    </table>

    <mat-paginator [pageSizeOptions]="[10, 15, 20]" showFirstLastButtons></mat-paginator>

</div>
<div style="margin-top: 20px;" *ngIf="!busquedaValida && busquedaValida != null" class="alert alert-warning" role="alert">
    !!! Hubo un error al consultar la tabla !!!
</div>`;

}

function generateColVar(json) {
    secondPartSearch = `
@Component({
selector: 'app-${json.table_name.toLowerCase()}search',
templateUrl: './${json.table_name.toLowerCase()}search.component.html',
styleUrls: ['./${json.table_name.toLowerCase()}search.component.css']
}) \n
export class ${json.table_name.toUpperCase()}SearchComponent implements OnInit { \n`

    let stringColumns = `displayedColumns: string[] = [`
    for (let i = 0; i < json.columns.length; i++) {
        if (i + 1 === json.columns.length) {
            stringColumns += `'${json.columns[i].Field || json.columns[i].field}', 'trash'];`
        } else {
            stringColumns += `'${json.columns[i].Field || json.columns[i].field}',`;
        }
    }
    secondPartSearch += stringColumns;
    thirdPartSearch = `\ndataSource;
    data;
    busquedaValida = null;
    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    
    constructor( public spinnerService: SpinnerService, public _dataTableService: DataTableService,
                 private router: Router, private _snackBar: MatSnackBar, private _snackBarConf: MatSnackBar) {
      this.doWork();
     }
    
    applyFilter(event: Event) {
      const filterValue = (event.target as HTMLInputElement).value;
      this.dataSource.filter = filterValue.trim().toLowerCase();
    }
    
    ngOnInit() {
    
    }\n`;

    thirdPartSearch += `
    
    openSnackBarDEL(data) {
        let style = 'valid-message';
        let snackBarRef = this._snackBarConf.open("¿Deseas elminar el registro?", "Si", {
          duration: 3000
        });
        snackBarRef.onAction().subscribe(()=> this.eliminar(data));
      }

    openSnackBar(valid, action) {
        let message;
        let style;
        if (valid){ 
        ${'message = `Se ${action} correctamente!!!!`;'}
        style = 'valid-message';
        } else { 
        ${'message = `Hubo un error al ${action} !!!!`;'}   
        style = 'invalid-message';
        }
        this._snackBar.open(message, '', {
          duration: 1500,
          horizontalPosition: 'right',
          verticalPosition: 'top',
          panelClass: [style]
        });
      }\n\n`;

    thirdPartSearch += `async doWork(){
    await this._dataTableService.getData${json.table_name.toUpperCase()}();
    this.data = this._dataTableService.data${json.table_name.toUpperCase()};
    this.busquedaValida = this._dataTableService.busquedaValida${json.table_name.toUpperCase()};
  
    if ( this.data != null && this.data !== ''){
      this.dataSource = new MatTableDataSource(this.data);
      this.dataSource.paginator = this.paginator;
    }
  
  }
  
  update(data){
  this._dataTableService.dataFiltered${json.table_name.toUpperCase()} = data;
  this.router.navigate(['/${json.table_name.toUpperCase()}Update']);
  }
  
  async eliminar(data){
    let json = {
        `
    json.columns.forEach(columna => {
        if(columna.Key === 'PRI' || columna.key === 'PRI'){
            thirdPartSearch += `${columna.Field || columna.field}:  data.${columna.Field || columna.field}, `
        }
    });       

thirdPartSearch +=  `};
      await this._dataTableService.deleteData${json.table_name.toUpperCase()}(json);
      if (this._dataTableService.deleteValido${json.table_name.toUpperCase()}){
        this.openSnackBar(true, 'elemino');
        setTimeout( () => {
          location.reload();
        }, 1500);
      } else {
        this.openSnackBar(false, 'elemino');
      }
  }

  Agregar(){
    this.router.navigate(['/${json.table_name.toUpperCase()}Add']);
  }
  
  }`;

}

const generateSearchTSFile = async(data, path) => {
    initialize();
    for (let i = 0; i < data.length; i++) {
        data[i].table_name = data[i].table_name.replace(/\s+/g, '');
        data[i].table_name = data[i].table_name.replace(/[^a-zA-Z0-9]/g, '');
        await generateColVar(data[i]);
        await generateHTML(data[i]);
        let htmlFull = htmlPartSearch + htmlSecondPart;
        let template = firstPartSearch + secondPartSearch + thirdPartSearch;
        await fse.outputFile(`${path}/MiProyecto/src/app/components/${data[i].table_name.toLowerCase()}/Search/${data[i].table_name.toLowerCase()}search.component.ts`, template)
            .catch(err => {
                return err;
            });
        await fse.outputFile(`${path}/MiProyecto/src/app/components/${data[i].table_name.toLowerCase()}/Search/${data[i].table_name.toLowerCase()}search.component.html`, htmlFull)
            .catch(err => {
                return err;
            });
        await fse.outputFile(`${path}//MiProyecto/src/app/components/${data[i].table_name.toLowerCase()}/Search/${data[i].table_name.toLowerCase()}search.component.css`, '')
            .catch(err => {
                return err;
            });
    }

}

exports.generateSearchTSFile = generateSearchTSFile;