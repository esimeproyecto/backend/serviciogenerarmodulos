const fse = require('fs-extra');
let firstPartEnvDes;
let firstPartEnvProd;

function initialize() {
    firstPartEnvDes = `export const environment = {
        production: false,
      `;
    firstPartEnvProd = `export const environment = {
        production: true,
      `

}



function generaEnvironments(data) {

    data.forEach(tabla => {
        console.log(tabla);
        firstPartEnvDes += ` host${tabla.replace(/[^a-zA-Z0-9]/g, '').toUpperCase()} : "http://localhost:3000/",
        getEndPoint${tabla.replace(/[^a-zA-Z0-9]/g, '').toUpperCase()}: "getEndPoint${tabla.toUpperCase()}",
        insertEndPoint${tabla.replace(/[^a-zA-Z0-9]/g, '').toUpperCase()}: "insertEndPoint${tabla.toUpperCase()}",
        updateEndPoint${tabla.replace(/[^a-zA-Z0-9]/g, '').toUpperCase()}: "updateEndPoint${tabla.toUpperCase()}",
        deleteEndPoint${tabla.replace(/[^a-zA-Z0-9]/g, '').toUpperCase()}: "deleteEndPoint${tabla.toUpperCase()}",
        `;
        firstPartEnvProd += ` host${tabla.replace(/[^a-zA-Z0-9]/g, '').toUpperCase()} : "http://localhost:3000/",
        getEndPoint${tabla.replace(/[^a-zA-Z0-9]/g, '').toUpperCase()}: "getEndPoint${tabla.toUpperCase()}",
        insertEndPoint${tabla.replace(/[^a-zA-Z0-9]/g, '').toUpperCase()}: "insertEndPoint${tabla.toUpperCase()}",
        updateEndPoint${tabla.replace(/[^a-zA-Z0-9]/g, '').toUpperCase()}: "updateEndPoint${tabla.toUpperCase()}",
        deleteEndPoint${tabla.replace(/[^a-zA-Z0-9]/g, '').toUpperCase()}: "deleteEndPoint${tabla.toUpperCase()}",
        `;
    });
    console.log(data);
    firstPartEnvDes += `\n};`

    firstPartEnvProd += `\n};`

}


const generateFile = (data, path) => {
    initialize();
    let tableNames = [];
    let fullTemplate;
    for (let i = 0; i < data.length; i++) {
        tableNames[i] = data[i].table_name;
    };
    generaEnvironments(tableNames);
    fse.outputFile(`${path}/MiProyecto/src/environments/environment.ts`, firstPartEnvDes)
        .catch(err => {
            return err;
        });
    fse.outputFile(`${path}/MiProyecto/src/environments/environment.prod.ts`, firstPartEnvProd)
        .catch(err => {
            return err;
        });
}

exports.generateFile = generateFile;