const fse = require('fs-extra');
let firstPart;
let secondPart;
let htmlPart;
let htmlSecondPart;

function initialize() {
    firstPart = `import { Component, OnInit, ɵConsole } from '@angular/core';
import { FormGroup, FormControl, Validators, RequiredValidator, AbstractControl} from '@angular/forms';
import { SpinnerService } from '../../../services/spinner-service.service';
import { DataTableService } from '../../../services/data-table.service';
import {MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
`;

    secondPart = ``;

    htmlPart = `<div class="container-fluid" [formGroup]="forma" style="margin-top: 20px;">
    
<div class="card ">`;

    htmlSecondPart = ``;
}



function generateHTML(json) {
    htmlSecondPart = ``;
    htmlSecondPart += `
        <div class="card-header" style="background-color: brown; color: white ;">
            Actualizar Campos
        </div>
        <div class="card-body">
            <div class="row">
            <div class="col-12">\n`;
    for (let i = 0; i < json.columns.length; i++) {
        let Type = json.columns[i].Type || json.columns[i].type;
        if (Type) {
            if (Type === 'date' || Type === 'datetime' || Type === 'timestamp') {
                htmlSecondPart += `<mat-form-field class=" espacio-relleno" style=" width: 50%;">
                <mat-label>${json.columns[i].Field || json.columns[i].field}</mat-label>
                <input formControlName="${json.columns[i].Field || json.columns[i].field}" matInput [matDatepicker]="picker${i}">
                <mat-datepicker-toggle matSuffix [for]="picker${i}"></mat-datepicker-toggle>
                <mat-datepicker #picker${i}></mat-datepicker>
            </mat-form-field>\n`;
            }
            if (Type.match(/[^()]+/g)[0] === 'char') {
                if (parseInt(Type.match(/[^()]+/g)[1], 10) >= 20) {
                    htmlSecondPart +=
                        `<mat-form-field class=" espacio-relleno" style=" width: 50%;">
                <mat-label>${json.columns[i].Field || json.columns[i].field}</mat-label>
                <textarea formControlName="${json.columns[i].Field || json.columns[i].field}" matInput></textarea>
            </mat-form-field>\n`;
                } else {
                    htmlSecondPart +=
                        `<mat-form-field class=" espacio-relleno" style=" width: 50%;">
                    <mat-label>${json.columns[i].Field || json.columns[i].field}</mat-label>
                    <input formControlName="${json.columns[i].Field || json.columns[i].field}" matInput>
                </mat-form-field>\n`;
                }

            } else {
                htmlSecondPart +=
                    `<mat-form-field class=" espacio-relleno" style=" width: 50%;">
                <mat-label>${json.columns[i].Field || json.columns[i].field}</mat-label>
                <input formControlName="${json.columns[i].Field || json.columns[i].field}" matInput>
            </mat-form-field>\n`;
            }

        } else {
            htmlSecondPart +=
                `<mat-form-field class=" espacio-relleno" style=" width: 50%;">
            <mat-label>${json.columns[i].Field || json.columns[i].field}</mat-label>
            <input formControlName="${json.columns[i].Field || json.columns[i].field}" matInput>
        </mat-form-field>\n`;
        }
    }
    htmlSecondPart += `
    </div>
    </div>

    </div>
    </div>
    <div class="container" style="margin-top: 20px;">
    <div class="text-right">
        <button mat-raised-button (click)="salir()" color="warn">Cancelar</button> &nbsp;
        <button mat-raised-button data-toggle="modal" data-target="#modal" (click)="insertaDatos()" [disabled]="!forma.valid" color="primary">
            <span matTooltip="Verifica los datos."  [matTooltipDisabled]="forma.valid">Guardar</span>
        </button>
    </div>
    </div>`


}


function generateUpdateTs(json) {
    secondPart = ``;
    secondPart += `
@Component({
selector: 'app-${json.table_name.toLowerCase()}update',
templateUrl: './${json.table_name.toLowerCase()}update.component.html',
styleUrls: ['./${json.table_name.toLowerCase()}update.component.css']
}) \n
export class ${json.table_name.toUpperCase()}UpdateComponent implements OnInit { \n
 forma: FormGroup;
 data;
 
 constructor( public spinnerService: SpinnerService, public _dataTableServices: DataTableService,
              private router: Router, private _snackBar: MatSnackBar) {
                this.forma = new FormGroup({ \n`;

    for (let i = 0; i < json.columns.length; i++) {

        let Type = json.columns[i].Type || json.columns[i];
        if (json.columns[i].Extra === 'auto_increment' || json.columns[i].extra === 'auto_increment') {
            secondPart += `${json.columns[i].Field || json.columns[i].field} : new FormControl({value: null, disabled : true} , [ `;
        } else {
            secondPart += `${json.columns[i].Field || json.columns[i].field} : new FormControl(null, [ `;
        }

        if (json.columns[i].Null != "YES" || json.columns[i].null != "YES") {
            secondPart += ` Validators.required, `;
        }
        if (Type.match(/[^()]+/g)[1]) {
            if (Type.match(/[^()]+/g)[0] === 'decimal') {
                secondPart += ` Validators.maxLength(${Type.match(/[^()]+/g)[1].split(',')[0] + Type.match(/[^()]+/g)[1].split(',')[1] }), `;
            } else {
                secondPart += ` Validators.maxLength(${Type.match(/[^()]+/g)[1]}), `;
            }
        }
        if (Type.match(/[^()]+/g)[0] === 'int' || Type.match(/[^()]+/g)[0] === 'integer' || Type.match(/[^()]+/g)[0] === 'double' || Type.match(/[^()]+/g)[0] === 'decimal') {
            secondPart += ` Validators.pattern("^[0-9]*$"), `;
        }
        secondPart += ` ]),\n`;
    }

    secondPart +=
        `});
this.doWork();
}

ngOnInit(): void {
}

openSnackBar(valid, action) {
    let message;
    let style;
    if (valid){ 
    ${'message = `Se ${action} correctamente!!!!`;'}
    style = 'valid-message';
    } else { 
    ${'message = `Hubo un error al ${action} !!!!`;'}   
    style = 'invalid-message';
    }
    this._snackBar.open(message, '', {
      duration: 1500,
      horizontalPosition: 'right',
      verticalPosition: 'top',
      panelClass: [style]
    });
  }\n\n

async doWork( ){ 
    this.data = await this._dataTableServices.dataFiltered${json.table_name.toUpperCase()};
    if (this.data != null || this.data !== '') {`;

    for (let i = 0; i < json.columns.length; i++) {
        secondPart += ` this.forma.get('${json.columns[i].Field || json.columns[i].field}').setValue(this.data.${json.columns[i].Field || json.columns[i].field});\n`;
    }

    secondPart +=
        `     }
}
async insertaDatos(){
    let json = {`;

    for (let i = 0; i < json.columns.length; i++) {
        secondPart += ` ${json.columns[i].Field || json.columns[i].field}: this.forma.get('${json.columns[i].Field || json.columns[i].field}').value,\n`;
    }

    secondPart +=

        ` };

    await this._dataTableServices.updateData${json.table_name.toUpperCase()}(json);

    if(this._dataTableServices.updateValido${json.table_name.toUpperCase()}){
        this.openSnackBar(true, 'actualizo');
        setTimeout( () => {
            this.salir();
          }, 1500);
    } else {
        this.openSnackBar(false, 'actualizo');
    }
}

salir(){
this.router.navigate(['/${json.table_name.toUpperCase()}Search']);
}

}`


}

const generateUpdateTSFile = async(data, path) => {
    initialize();
    for (let i = 0; i < data.length; i++) {
        data[i].table_name = data[i].table_name.replace(/\s+/g, '');
        data[i].table_name = data[i].table_name.replace(/[^a-zA-Z0-9]/g, '');
        generateHTML(data[i]);
        generateUpdateTs(data[i]);
        template = firstPart + secondPart;
        htmlFull = htmlPart + htmlSecondPart;
        await fse.outputFile(`${path}/MiProyecto/src/app/components/${data[i].table_name.toLowerCase()}/Update/${data[i].table_name.toLowerCase()}update.component.ts`, template)
            .catch(err => {
                return err;
            });
        await fse.outputFile(`${path}/MiProyecto/src/app/components/${data[i].table_name.toLowerCase()}/Update/${data[i].table_name.toLowerCase()}update.component.html`, htmlFull)
            .catch(err => {
                return err;
            });
        await fse.outputFile(`${path}/MiProyecto/src/app/components/${data[i].table_name.toLowerCase()}/Update/${data[i].table_name.toLowerCase()}update.component.css`, '')
            .catch(err => {
                return err;
            });
    }

}

exports.generateUpdateTSFile = generateUpdateTSFile;