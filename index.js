const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const dataTableServices = require('./dataTableServices');
const componentSearch = require('./componentsSearch');
const componentUpdate = require('./componentsUpdate');
const componentAdd = require('./componentsAdd');
const environments = require('./environments');
const fs = require('fs');
var uuid = require('uuid-random');
var cors = require('cors');
var zip = require('bestzip');


const port = process.env.PORT || 3300;


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

app.post('/generarModulos', async function(req, res) {
    let data = req.body.json;
    let absolutPath = req.body.path;
    try {
        //genera los componentes de Busqueda
        // console.log(data);
        // console.log(req.body.path);
        environments.generateFile(data, absolutPath);
        dataTableServices.generateFile(data, absolutPath);
        componentSearch.generateSearchTSFile(data, absolutPath);
        componentUpdate.generateUpdateTSFile(data, absolutPath);
        componentAdd.generateAddTSFile(data, absolutPath);
        // var idUnico = uuid();
        // await zip({
        //     source: `${absolutPath}/MiProyecto/*`,
        //     destination: `${absolutPath}.zip`
        // }).then(function() {
        //     console.log('all done!');
        // }).catch(function(err) {
        //     console.error(err.stack);
        //     process.exit(1);
        // });
    } catch (err) {
        res.json({ status: false });
        throw console.log(err);
    }
    res.json({ status: true });
});

app.listen(port, function() {
    console.log('El sitio de APIs inició correctamente en el puerto: ', port);
});