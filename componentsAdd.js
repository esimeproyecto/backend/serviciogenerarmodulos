const fse = require('fs-extra');
const { type } = require('os');
let firstPartAdd;
let secondPartAdd;
let htmlPartAdd;
let htmlSecondAddPart;

function initialize() {
    firstPartAdd = `import { Component, OnInit, ɵConsole } from '@angular/core';
import { FormGroup, FormControl, Validators, RequiredValidator, AbstractControl} from '@angular/forms';
import { SpinnerService } from '../../../services/spinner-service.service';
import { DataTableService } from '../../../services/data-table.service';
import {MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
`;

    secondPartAdd = ``;

    htmlPartAdd = `<div class="container-fluid" [formGroup]="forma" style="margin-top: 20px;">
    
<div class="card ">`;

    htmlSecondAddPart = ``;
}



function generateHTML(json) {
    htmlSecondAddPart = ``;
    htmlSecondAddPart += `
        <div class="card-header" style="background-color: brown; color: white ;">
            Agregar un nuevo registro
        </div>
        <div class="card-body">
            <div class="row">
            <div class="col-12">\n`;
    for (let i = 0; i < json.columns.length; i++) {
        let Type = json.columns[i].Type || json.columns[i].type;
        if (Type) {
            if (Type === 'date' || Type === 'datetime' || Type === 'timestamp') {
                htmlSecondAddPart += `<mat-form-field class=" espacio-relleno" style=" width: 50%;">
                    <mat-label>${json.columns[i].Field || json.columns[i].field}</mat-label>
                    <input formControlName="${json.columns[i].Field || json.columns[i].field}" matInput [matDatepicker]="picker${i}">
                    <mat-datepicker-toggle matSuffix [for]="picker${i}"></mat-datepicker-toggle>
                    <mat-datepicker #picker${i}></mat-datepicker>
                </mat-form-field>\n`;
            } else if (Type.match(/[^()]+/g)[0] === 'char' || Type.match(/[^()]+/g)[0] === 'varchar' || Type.match(/[^()]+/g)[0] === 'blob') {
                if (parseInt(Type.match(/[^()]+/g)[1], 10) >= 20) {
                    htmlSecondAddPart +=
                        `<mat-form-field class=" espacio-relleno" style=" width: 50%;">
                    <mat-label>${json.columns[i].Field || json.columns[i].field}</mat-label>
                    <textarea formControlName="${json.columns[i].Field || json.columns[i].field}" matInput></textarea>
                </mat-form-field>\n`;
                } else {
                    htmlSecondAddPart +=
                        `<mat-form-field class=" espacio-relleno" style=" width: 50%;">
                        <mat-label>${json.columns[i].Field || json.columns[i].field}</mat-label>
                        <input formControlName="${json.columns[i].Field || json.columns[i].field}" matInput>
                    </mat-form-field>\n`;
                }

            } else {
                htmlSecondAddPart +=
                    `<mat-form-field class=" espacio-relleno" style=" width: 50%;">
                    <mat-label>${json.columns[i].Field || json.columns[i].field}</mat-label>
                    <input formControlName="${json.columns[i].Field || json.columns[i].field}" matInput>
                </mat-form-field>\n`;
            }

        } else {
            htmlSecondAddPart +=
                `<mat-form-field class=" espacio-relleno" style=" width: 50%;">
                <mat-label>${json.columns[i].Field || json.columns[i].field}</mat-label>
                <input formControlName="${json.columns[i].Field || json.columns[i].field}" matInput>
            </mat-form-field>\n`;
        }
    }
    htmlSecondAddPart += `
    </div>
    </div>

    </div>
    </div>
    <div class="container" style="margin-top: 20px;">
    <div class="text-right">
        <button mat-raised-button (click)="salir()" color="warn">Cancelar</button> &nbsp;
        <button mat-raised-button data-toggle="modal" data-target="#modal" (click)="insertaDatos()" [disabled]="!forma.valid" color="primary">
            <span matTooltip="Verifica los datos."  [matTooltipDisabled]="forma.valid">Guardar</span>
        </button>
    </div>
    </div>`


}


function generateAddTs(json) {
    secondPartAdd = ``;
    secondPartAdd += `
@Component({
selector: 'app-${json.table_name.toLowerCase()}add',
templateUrl: './${json.table_name.toLowerCase()}add.component.html',
styleUrls: ['./${json.table_name.toLowerCase()}add.component.css']
}) \n
export class ${json.table_name.toUpperCase()}AddComponent implements OnInit { \n
 forma: FormGroup;
 data;
 
 constructor( public spinnerService: SpinnerService, public _dataTableServices: DataTableService,
              private router: Router, private _snackBar: MatSnackBar) {
                this.forma = new FormGroup({ \n`;

    for (let i = 0; i < json.columns.length; i++) {

        let Type = json.columns[i].Type || json.columns[i];
        if (json.columns[i].Extra === 'auto_increment' || json.columns[i].extra === 'auto_increment') {
            secondPartAdd += `${json.columns[i].Field || json.columns[i].field} : new FormControl({value: null, disabled : true} , [ `;
        } else {
            secondPartAdd += `${json.columns[i].Field || json.columns[i].field} : new FormControl(null, [ `;
        }

        if (json.columns[i].Null != "YES") {
            secondPartAdd += ` Validators.required, `;
        }
        if (Type.match(/[^()]+/g)[1]) {
            if (Type.match(/[^()]+/g)[0] === 'decimal') {
                secondPartAdd += ` Validators.maxLength(${Type.match(/[^()]+/g)[1].split(',')[0] + Type.match(/[^()]+/g)[1].split(',')[1] }), `;
            } else {
                secondPartAdd += `Validators.maxLength(${Type.match(/[^()]+/g)[1]}), `;
            }
        }
        if (Type.match(/[^()]+/g)[0] === 'int' || Type.match(/[^()]+/g)[0] === 'integer' || Type.match(/[^()]+/g)[0] === 'double' || Type.match(/[^()]+/g)[0] === 'decimal') {
            secondPartAdd += ` Validators.pattern("^[0-9]*$"), `;
        }
        secondPartAdd += ` ]),\n`;
    }

    secondPartAdd +=
        `});
}

ngOnInit(): void {
}

openSnackBar(valid, action) {
    let message;
    let style;
    if (valid){ 
    ${'message = `Se ${action} correctamente!!!!`;'}
    style = 'valid-message';
    } else { 
    ${'message = `Hubo un error al ${action} !!!!`;'}   
    style = 'invalid-message';
    }
    this._snackBar.open(message, '', {
      duration: 1500,
      horizontalPosition: 'right',
      verticalPosition: 'top',
      panelClass: [style]
    });
  }\n\n

async insertaDatos(){
    let json = {`;

    for (let i = 0; i < json.columns.length; i++) {
        secondPartAdd += ` ${json.columns[i].Field || json.columns[i].field}: this.forma.get('${json.columns[i].Field || json.columns[i].field}').value,\n`;
    }

    secondPartAdd +=

        ` };

    await this._dataTableServices.insertData${json.table_name.toUpperCase()}(json);

    if(this._dataTableServices.insertValido${json.table_name.toUpperCase()}){
        this.openSnackBar(true, 'inserto');
        setTimeout( () => {
            this.salir();
          }, 1500);
    } else {
        this.openSnackBar(false, 'inserto');
    }
}

salir(){
this.router.navigate(['/${json.table_name.toUpperCase()}Search']);
}

}`


}

const generateAddTSFile = async(data, path) => {
    initialize();
    for (let i = 0; i < data.length; i++) {
        data[i].table_name = data[i].table_name.replace(/\s+/g, '');
        data[i].table_name = data[i].table_name.replace(/[^a-zA-Z0-9]/g, '');
        generateHTML(data[i]);
        generateAddTs(data[i]);
        templateAdd = firstPartAdd + secondPartAdd;
        htmlFullAdd = htmlPartAdd + htmlSecondAddPart;
        await fse.outputFile(`${path}/MiProyecto/src/app/components/${data[i].table_name.toLowerCase()}/Add/${data[i].table_name.toLowerCase()}add.component.ts`, templateAdd)
            .catch(err => {
                return err;
            });
        await fse.outputFile(`${path}/MiProyecto/src/app/components/${data[i].table_name.toLowerCase()}/Add/${data[i].table_name.toLowerCase()}add.component.html`, htmlFullAdd)
            .catch(err => {
                return err;
            });
        await fse.outputFile(`${path}/MiProyecto/src/app/components/${data[i].table_name.toLowerCase()}/Add/${data[i].table_name.toLowerCase()}add.component.css`, '')
            .catch(err => {
                return err;
            });
    }

}

exports.generateAddTSFile = generateAddTSFile;