const fse = require('fs-extra');
let firstPart;
let secondPart;

function initialize() {
    firstPart = `import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DataTableService {
`;

    secondPart = ``;
}



function generaServices(data) {

    data.forEach(tabla => {
        secondPart +=
            `\ndata${tabla.toUpperCase()};
busquedaValida${tabla.toUpperCase()}: boolean;
dataFiltered${tabla.toUpperCase()};
dataUpdate${tabla.toUpperCase()};
updateValido${tabla.toUpperCase()}: boolean;
dataInsert${tabla.toUpperCase()};
insertValido${tabla.toUpperCase()}: boolean;
dataDelete${tabla.toUpperCase()};
deleteValido${tabla.toUpperCase()}: boolean;`;
    });

    secondPart += `\nconstructor(private http: HttpClient) {
    }`;

    data.forEach(tabla => {

        secondPart +=
            `\n
    async getData${tabla.toUpperCase()}(){
    this.data${tabla.toUpperCase()} = null;
    this.busquedaValida${tabla.toUpperCase()} = false;
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    await this.http.post( environment.host${tabla.toUpperCase()} + environment.getEndPoint${tabla.toUpperCase()} , {}, { headers: headers }).toPromise().then(
      res => {
        this.data${tabla.toUpperCase()} = res;
        this.busquedaValida${tabla.toUpperCase()} = true;
      }, err => {
        this.busquedaValida${tabla.toUpperCase()} = false;
      });
  }
  
  async insertData${tabla.toUpperCase()}(json){
    this.dataInsert${tabla.toUpperCase()} = null;
    this.insertValido${tabla.toUpperCase()} = false;
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    await this.http.post( environment.host${tabla.toUpperCase()}  + environment.insertEndPoint${tabla.toUpperCase()} , json , { headers: headers }).toPromise().then(
      res => {
        this.dataInsert${tabla.toUpperCase()} = res;
        if(this.dataInsert${tabla.toUpperCase()}.affectedRows === 1){
          this.insertValido${tabla.toUpperCase()} = true;
        }else {
          this.insertValido${tabla.toUpperCase()} = false;
        }
      }, err => {
        this.insertValido${tabla.toUpperCase()} = false;
      });
    }
  
  async updateData${tabla.toUpperCase()}(json){
    this.dataUpdate${tabla.toUpperCase()} = null;
    this.updateValido${tabla.toUpperCase()} = false;
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    await this.http.post( environment.host${tabla.toUpperCase()} + environment.updateEndPoint${tabla.toUpperCase()} , json , { headers: headers }).toPromise().then(
      res => {
        this.dataUpdate${tabla.toUpperCase()} = res;
        if(this.dataUpdate${tabla.toUpperCase()}.affectedRows === 1){
          this.updateValido${tabla.toUpperCase()} = true;
        }else {
          this.updateValido${tabla.toUpperCase()} = false;
        }
      }, err => {
        this.updateValido${tabla.toUpperCase()} = false;
      });
    }
  
  async deleteData${tabla.toUpperCase()}(json){
    this.dataDelete${tabla.toUpperCase()} = null;
    this.deleteValido${tabla.toUpperCase()} = false;
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    await this.http.post( environment.host${tabla.toUpperCase()} + environment.deleteEndPoint${tabla.toUpperCase()} , json , { headers: headers }).toPromise().then(
      res => {
        this.dataDelete${tabla.toUpperCase()} = res;
        if(this.dataDelete${tabla.toUpperCase()}.affectedRows === 1){
          this.deleteValido${tabla.toUpperCase()} = true;
        }else {
          this.deleteValido${tabla.toUpperCase()} = false;
        }
      }, err => {
        this.deleteValido${tabla.toUpperCase()} = false;
      });
    }
  
  `
    });

    secondPart += `\n }`;

}


const generateFile = (data, path) => {
    initialize();
    let tableNames = [];
    let fullTemplate;
    for (let i = 0; i < data.length; i++) {
        data[i].table_name = data[i].table_name.replace(/\s+/g, '');
        data[i].table_name = data[i].table_name.replace(/[^a-zA-Z0-9]/g, '');
        tableNames[i] = data[i].table_name;
    };
    generaServices(tableNames);
    fullTemplate = firstPart + secondPart;
    fse.outputFile(`${path}/MiProyecto/src/app/services/data-table.service.ts`, fullTemplate)
        .catch(err => {
            return err;
        });
}

exports.generateFile = generateFile;